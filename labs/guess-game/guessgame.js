var waitMessage = "Waiting for a number ..."
var tooHighMessage = "Too high! Guess again";
var tooLowMessage = "Too low! Guess again";
var gotItMessage = "You got it!";

swapMessage(gotItMessage); //function call
swapMessage(tooHighMessage);
swapMessage(tooLowMessage);
swapMessage(waitMessage);

//function definition
function swapMessage(messageText) {
    var messageElement = document.getElementById("message");
    var messageNode = document.createTextNode(messageText);
    messageElement.removeChild(messageElement.childNodes[0]);
    messageElement.appendChild(messageNode);
}

