var guess = document.getElementById('guess');
var result = document.getElementById('result');
var test = document.getElementById('test');

function random(min,max) {
  var num = Math.floor(Math.random()*(max-min)) + min;
  return num;
}

var target = random(1,50);

test.onclick = function() {
    
    if(guess.value>target) {
        result.innerHTML = "too high";
    } else if (guess.value<target) {
        result.innerHTML = "too low";
    } else {
        result.innerHTML = "correct";
    }
}

