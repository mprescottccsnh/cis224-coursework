//Dimension the canvas
width = 640;
height = 480;

var setup = function () {

};

//Function to display number of attempts left
var attemptsLeft = function () {

};

var drawButton = function () {

};

var isButtonClicked = function (x, y) {

}

var emoji = function (emotion) {
    var x=width/2;
    var y=250;
    var size = 80;
    //draw head
    fill(255, 255, 0);
    ellipse(x,y,size,size);
    
    //draw facial features
    fill(0, 0, 0);
    ellipse(x-10,y-5,size/10,size/10);
    ellipse(x+10,y-5,size/10,size/10);
    
    noFill();
    strokeWeight(2);
    
    if(emotion === "happy") {
        line(x-10,y+12,x,y+20);
        line(x,y+20,x+10,y+12);
    } else if (emotion === "sad") {
        line(x-10,y+20,x,y+12);
        line(x,y+12,x+10,y+20);
    } else {
        line(x-size/7*2,y+7,x+size/7*2,y+2);
    }
};


mouseClicked = function () {

}

setup();






