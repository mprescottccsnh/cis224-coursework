// setup canvas

var canvas = document.querySelector('canvas');
var ctx = canvas.getContext('2d');

var width = canvas.width = window.innerWidth;
var height = canvas.height = window.innerHeight;

// function to generate random number

function random(min,max) {
  var num = Math.floor(Math.random()*(max-min)) + min;
  return num;
}

// define Ball constructor
function Ball(x, y, velX, velY, color, size) {
    this.x = x;
    this.y = y;
    this.velX = velX;
    this.velY = velY;
    this.color = color;
    this.size = size;
}

// define ball draw method
Ball.prototype.draw = function() {
    ctx.beginPath();
    ctx.fillStyle = this.color;
    ctx.arc(this.x, this.y, this.size, 0, 2*Math.PI);
    ctx.fill();
};

//var b = new Ball(200,100,5,5,'rgb(220,200,0)',10);
//b.draw();

// define ball update method
Ball.prototype.update = function() {
    if ((this.x >= width) || (this.x <= 0)) {
        this.velX = -(this.velX);
    }
    
    if ((this.y >= height) || (this.y <= 0)) {
        this.velY = -(this.velY);
    }
    
    this.x += this.velX;
    this.y += this.velY;
};

// define ball collision detection
Ball.prototype.collisionDetect = function() {
    for (var i=0; i<balls.length; i++) {
        var other = balls[i];
        
        if(!(this === other)) {
            var dx = this.x - other.x;
            var dy = this.y - other.y;
            var distance = Math.sqrt((dx*dx) + (dy*dy));
            
            if(distance < this.size + other.size) {
                other.color = this.color = 'rgb(' + random(0,255) + ',' + random(0,255) + ',' + random(0,255) + ')';
            }
        }
    }
};

// define array to store balls
var balls = [];

// define loop that keeps drawing the scene constantly
function loop() {
    ctx.fillStyle = 'rgba(0,0,0,0.25)';
    ctx.fillRect(0,0,width,height);
    while (balls.length < 25) {
        var ball = new Ball(
            random(0,width),
            random(0,height),
            random(-7,7),
            random(-7,7),
            'rgb(' + random(0,255) + ',' + random(0,255) + ',' + random(0,255) + ')',
            random(10,20)
        );
        
        balls.push(ball);
    }
    
    for (var i=0; i<balls.length; i++) {
        balls[i].draw();
        balls[i].update();
        balls[i].collisionDetect();
    }
    
    requestAnimationFrame(loop);
}

// Finally, call the loop function to get the balls bouncing

loop();








