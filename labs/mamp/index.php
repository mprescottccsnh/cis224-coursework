<!DOCTYPE html>
<?php
    require('dbconnect.php');
    
    if($dbconn->connect_error) {
        die('Something wrong with the database ...');
    }
    //$dbconn does exist and provides access to the myblog database

    $rows = $dbconn->query('SELECT post, post_date FROM posts WHERE id=1;');
    $postRec = $rows->fetch_assoc();
    
?><html>
    <head></head>

    <body>
        <h1>Welcome to My Blog</h1>
        <?php
        echo "<p>" . $postRec['post'] . "</p>";
        echo "<p><em>" . $postRec['post_date'] . "</em></p>";
        ?>
        
    </body>
</html>