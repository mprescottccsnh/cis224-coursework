const {createServer} = require("http");

let server = createServer(
    (request, response) => {
        response.writeHead(200, {"Content-type": "text/html"});
        response.write(`<h1>Hello!</h1><p>You asked for <b>${request.url}</b>`);
        response.end();
    }
);

server.listen(8080);
