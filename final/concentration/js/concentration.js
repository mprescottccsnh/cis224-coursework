ConcentrationApp = function() {
    var self = this;

    this.deck = [];
    this.card1 = null;
    this.card2 = null;
    this.numberMatch = null;
    this.possibleMatches = null;
    this.board = document.getElementById('concentrationgame');
    
    this.click = function(event) {
        //Grab the event target and check to see what card was clicked
        var target = (function(t){
            for(var i=0; i<self.deck.length; i++) {
                if(t === self.deck[i].html) {
                    return self.deck[i];
                }
            }
        }(event.target));
        //console.log(target);
        
      //Game logic
      //If the card clicked is selected
       if (this.classList.contains('selected') === true) {
          //If no card has been chosen yet
          if (self.card1 === null && self.card2 === null) {
             self.card1 = target;
          } else if (self.card1 !== null && self.card2 === null) {
             self.card2 = target;
             
             //Check for a match
             if (self.card1.value === self.card2.value) {
                self.cardMatch();
             } else {
                self.cardMismatch();
             }
          }
       } 
       
    };
    
    this.init();
};

ConcentrationApp.prototype.init = function() {
    this.numberMatch = 0;
    this.possibleMatches = cards.length;
   
    for (var j=0; j<2; j++) {
        for (var i=0; i<cards.length; i++) {
            this.deck.push(new Util.Card(i));
        }
    }
    
    Util.shuffle(this.deck);
    
    //Get the cards into the game div in the HTML
    for (var i=0; i<this.deck.length; i++) {
        this.board.append(this.deck[i].html);
        this.deck[i].html.addEventListener('click', this.click);
    } 
};

ConcentrationApp.prototype.cardMatch = function() {
   var self = this;
   self.numberMatch += 1;
   
   window.setTimeout(function() {
      self.card1.html.classList.add('matched');
      self.card2.html.classList.add('matched');
   }, 200);
   
   window.setTimeout(function() {
      self.removeSelected(self); 
   }, 500);
   
   if(self.possibleMatches === self.numberMatch) {
      var messageBox = document.getElementById('message');
      messageBox.innerHTML = "Yay!! You win!!";
   }
};

ConcentrationApp.prototype.cardMismatch = function() {
   var self = this;

   //Change the card state back to being unselected
   window.setTimeout(function() {
      self.removeSelected(self);
   }, 500);

};

ConcentrationApp.prototype.removeSelected = function(app) {
      app.card1.html.classList.remove('selected');
      app.card2.html.classList.remove('selected');
      
      app.card1 = null;
      app.card2 = null;   
}

var game = new ConcentrationApp();















